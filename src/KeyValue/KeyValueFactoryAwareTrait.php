<?php

namespace Drupal\awareness\KeyValue;

/**
 * Trait for classes that utilize the keyvalue service.
 */
trait KeyValueFactoryAwareTrait {

  /**
   * Get the key value factory service.
   *
   * @return \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   *   The key value factory service.
   */
  protected function getKeyValueFactory() {
    return \Drupal::service('keyvalue');
  }

}
