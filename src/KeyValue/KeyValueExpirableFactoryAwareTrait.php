<?php

namespace Drupal\awareness\KeyValue;

/**
 * Trait for classes that utilize the keyvalue.expirable service.
 */
trait KeyValueExpirableFactoryAwareTrait {

  /**
   * Get the key value expirable factory service.
   *
   * @return \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface
   *   The key value expirable factory service.
   */
  protected function getKeyValyeExpirableFactory() {
    return \Drupal::service('keyvalue.expirable');
  }

}
