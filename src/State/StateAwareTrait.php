<?php declare(strict_types = 1);

namespace Drupal\awareness\State;

use \Drupal\Core\State\StateInterface;

/**
 * Trait for classes that utilize the state service.
 */
trait StateAwareTrait {

  /**
   * Get the state service.
   *
   * @return \Drupal\Core\State\StateInterface
   *   The state service.
   */
  protected function getState(): StateInterface {
    return \Drupal::service('state');
  }

}
