<?php

namespace Drupal\awareness\DateTime;

/**
 * Trait for classes that utilize date.formatter service.
 */
trait DateFormatterAwareTrait {

  /**
   * Get the date formatter service.
   *
   * @return \Drupal\Core\Datetime\DateFormatterInterface
   *   The date formatter service.
   */
  protected function getDateFormatter() {
    return \Drupal::service('date.formatter');
  }

}
