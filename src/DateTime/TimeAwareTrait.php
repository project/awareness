<?php

namespace Drupal\awareness\DateTime;

/**
 * Trait for classes that utilize datetime.time service.
 */
trait TimeAwareTrait {

  /**
   * Get the time service.
   *
   * @return \Drupal\Component\Datetime\TimeInterface
   *   The time service.
   */
  protected function getTime() {
    return \Drupal::time();
  }

}
