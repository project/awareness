<?php

declare(strict_types = 1);

namespace Drupal\awareness\Token;

use Drupal\Core\Utility\Token;
use Drupal\token\TokenInterface;

/**
 * Trait for classes that utilize the token service.
 */
trait TokenAwareTrait {

  /**
   * Get the token service.
   *
   * @return \Drupal\Core\Utility\Token|\Drupal\token\TokenInterface
   *   The token service.
   */
  protected function getToken(): Token|TokenInterface {
    return \Drupal::token();
  }

}
