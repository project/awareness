<?php

declare(strict_types = 1);

namespace Drupal\awareness\Lock;

use \Drupal\Core\Lock\LockBackendInterface;

/**
 * Trait for classes that utilize the lock service.
 */
trait LockAwareTrait {

  /**
   * Get the lock service.
   *
   * @return \Drupal\Core\Lock\LockBackendInterface
   *   The lock service.
   */
  protected function getLock(): LockBackendInterface {
    return \Drupal::service('lock');
  }

}
