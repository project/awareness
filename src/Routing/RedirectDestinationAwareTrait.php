<?php

namespace Drupal\awareness\Routing;

/**
 * Trait for classes that utilize redirect.destination service.
 */
trait RedirectDestinationAwareTrait {

  /**
   * Get the redirect destination service.
   *
   * @return \Drupal\Core\Routing\RedirectDestinationInterface
   *   The redirect destination service.
   */
  protected function getRedirectDestination() {
    return \Drupal::service('redirect.destination');
  }

}
