<?php

namespace Drupal\awareness\Routing;

/**
 * Trait for classes that utilize current_route_match service.
 */
trait RouteMatchAwareTrait {

  /**
   * Get the current route match.
   *
   * @return \Drupal\Core\Routing\RouteMatchInterface
   *   The current route match.
   */
  protected function getRouteMatch() {
    return \Drupal::routeMatch();
  }

}
