<?php

namespace Drupal\awareness\Render;

/**
 * Trait for classes that utilize renderer service.
 */
trait RendererAwareTrait {

  /**
   * Get the renderer service.
   *
   * @return \Drupal\Core\Render\RendererInterface
   *   The renderer service.
   */
  protected function getRenderer() {
    return \Drupal::service('renderer');
  }

}
