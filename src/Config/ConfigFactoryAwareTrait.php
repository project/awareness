<?php

namespace Drupal\awareness\Config;

/**
 * Trait for classes that utilize config.factory service.
 */
trait ConfigFactoryAwareTrait {

  /**
   * Load a config object.
   *
   * @param string $name
   *   The name of the configuration object.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The config object.
   */
  protected function getConfig($name) {
    return \Drupal::config($name);
  }

  /**
   * Load an editable config object.
   *
   * @param string $name
   *   The name of the configuration object.
   *
   * @return \Drupal\Core\Config\Config
   *   The editable config object.
   */
  protected function getEditableConfig($name) {
    return \Drupal::service('config.factory')->getEditable($name);
  }

}
