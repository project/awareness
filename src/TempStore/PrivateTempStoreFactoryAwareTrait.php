<?php

namespace Drupal\awareness\TempStore;

/**
 * Trait for classes that utilize the tempstore.private service.
 */
trait PrivateTempStoreFactoryAwareTrait {

  /**
   * Get the private temporary storage factory service.
   *
   * @return \Drupal\Core\TempStore\PrivateTempStoreFactory
   *   The private temporary storage factory service.
   */
  protected function getPrivateTempStoreFactory() {
    return \Drupal::service('tempstore.private');
  }

}
