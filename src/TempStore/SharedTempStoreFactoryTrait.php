<?php

namespace Drupal\awareness\TempStore;

/**
 * Trait for classes that utilize the tempstore.shared service.
 */
trait SharedTempStoreFactoryTrait {

  /**
   * Get the shared temporary storage factory service.
   *
   * @return \Drupal\Core\TempStore\SharedTempStoreFactory
   *   The shared temporary storage factory service.
   */
  protected function getSharedTempStoreFactory() {
    return \Drupal::service('tempstore.shared');
  }

}
