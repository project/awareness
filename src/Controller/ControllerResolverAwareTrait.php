<?php

namespace Drupal\awareness\Controller;

/**
 * Trait for classes that utilize controller_resolver service.
 */
trait ControllerResolverAwareTrait {

  /**
   * Get the controller resolver service.
   *
   * @return \Drupal\Core\Controller\ControllerResolverInterface
   *   The controller resolver service.
   */
  protected function getControllerResolver() {
    return \Drupal::service('controller_resolver');
  }

}
