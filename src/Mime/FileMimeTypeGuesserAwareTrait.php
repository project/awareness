<?php

declare(strict_types = 1);

namespace Drupal\awareness\Mime;

use \Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Trait for classes that utilize the file.mime_type.guesser service.
 */
trait FileMimeTypeGuesserAwareTrait {

  /**
   * Get the file.mime_type.guesser service.
   *
   * @return \Symfony\Component\Mime\MimeTypeGuesserInterface
   *   The file.mime_type.guesser service.
   */
  protected function getFileMimeTypeGuesser(): MimeTypeGuesserInterface {
    return \Drupal::service('file.mime_type.guesser');
  }

}
