<?php

declare(strict_types = 1);

namespace Drupal\awareness\StreamWrapper;

use \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;

/**
 * Trait for classes that utilize the stream_wrapper_manager service.
 */
trait StreamWrapperManagerAwareTrait {

  /**
   * Get the stream_wrapper_manager service.
   *
   * @return \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   *   The stream_wrapper_manager service.
   */
  protected function getStreamWrapperManager(): StreamWrapperManagerInterface {
    return \Drupal::service('stream_wrapper_manager');
  }

}
