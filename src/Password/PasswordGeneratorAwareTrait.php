<?php

namespace Drupal\awareness\Password;

/**
 * Trait for classes that utilize password_generator service.
 */
trait PasswordGeneratorAwareTrait {

  /**
   * Get the password generator service.
   *
   * @return \Drupal\Core\Password\PasswordGeneratorInterface
   *   The password generator service.
   */
  protected function getPasswordGenerator() {
    return \Drupal::service('password_generator');
  }

}
