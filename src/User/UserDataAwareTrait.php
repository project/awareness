<?php

declare(strict_types = 1);

namespace Drupal\awareness\User;

use \Drupal\user\UserDataInterface;

/**
 * Trait for classes that utilize the user.data service.
 */
trait UserDataAwareTrait {

  /**
   * Get the user.data service.
   *
   * @return \Drupal\user\UserDataInterface
   *   The user.data service.
   */
  protected function getUserData(): UserDataInterface {
    return \Drupal::service('user.data');
  }

}
