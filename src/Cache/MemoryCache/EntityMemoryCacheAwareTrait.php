<?php declare(strict_types = 1);

namespace Drupal\awareness\Cache\MemoryCache;

use \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;

/**
 * Trait for classes that utilize the entity.memory_cache service.
 */
trait EntityMemoryCacheAwareTrait {

  /**
   * Get the entity.memory_cache service.
   *
   * @return \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface
   *   The entity.memory_cache service.
   */
  public function getEntityMemoryCache(): MemoryCacheInterface {
    return \Drupal::service('entity.memory_cache');
  }

}
