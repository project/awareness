<?php

namespace Drupal\awareness\Cache;

use Drupal\Core\Cache\CacheFactoryInterface;

/**
 * Trait for classes that utilize cache.factory service.
 */
trait CacheFactoryAwareTrait {

  /**
   * Returns the requested cache bin.
   *
   * @param string $bin
   *   (optional) The cache bin for which the cache object should be returned,
   *   defaults to 'default'.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The cache object associated with the specified bin.
   */
  protected function getCacheBin($bin = 'default') {
    return \Drupal::cache($bin);
  }

}
