<?php

namespace Drupal\awareness\Event;

/**
 * Trait for classes that utilize event_dispatcher service.
 */
trait EventDispatcherAwareTrait {

  /**
   * Get the event dispatcher.
   *
   * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
   *   The event dispatcher.
   */
  protected function getEventDispatcher() {
    return \Drupal::service('event_dispatcher');
  }

}
