<?php

namespace Drupal\awareness\Database;

/**
 * Trait for classes that utilize database service.
 */
trait DatabaseAwareTrait {

  /**
   * Get the database service.
   *
   * @return \Drupal\Core\Database\Connection
   *   The database service.
   */
  protected function getDatabase() {
    return \Drupal::database();
  }

}
