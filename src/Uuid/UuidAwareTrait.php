<?php

namespace Drupal\awareness\Uuid;

/**
 * Trait for classes that utilize the uuid service.
 */
trait UuidAwareTrait {

  /**
   * Get the UUID service.
   *
   * @return \Drupal\Component\Uuid\UuidInterface
   *   The UUID service.
   */
  protected function getUuid() {
    return \Drupal::service('uuid');
  }

}
