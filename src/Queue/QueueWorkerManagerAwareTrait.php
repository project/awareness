<?php

namespace Drupal\awareness\Queue;

/**
 * Trait for classes that utilize plugin.manager.queue_worker service.
 */
trait QueueWorkerManagerAwareTrait {

  /**
   * Get the queue worker manager service.
   *
   * @return \Drupal\Core\Queue\QueueWorkerManagerInterface
   *   The queue worker manager service.
   */
  protected function getQueueWorkerManager() {
    return \Drupal::service('plugin.manager.queue_worker');
  }

}
