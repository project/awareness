<?php

namespace Drupal\awareness\Queue;

/**
 * Trait for classes that utilize queue service.
 */
trait QueueFactoryAwareTrait {

  /**
   * Get the queue factory.
   *
   * @return \Drupal\Core\Queue\QueueFactory
   *   The queue factory.
   */
  protected function getQueueFactory() {
    return \Drupal::service('queue');
  }

}
