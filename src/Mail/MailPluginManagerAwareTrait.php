<?php

declare(strict_types = 1);

namespace Drupal\awareness\Mail;

use \Drupal\Core\Mail\MailManagerInterface;

/**
 * Trait for classes that utilize the plugin.manager.mail service.
 */
trait MailPluginManagerAwareTrait {

  /**
   * Get the plugin.manager.mail service.
   *
   * @return \Drupal\Core\Mail\MailManagerInterface
   *   The plugin.manager.mail service.
   */
  protected function getMailPluginManager(): MailManagerInterface {
    return \Drupal::service('plugin.manager.mail');
  }

}
