<?php

namespace Drupal\awareness\Form;

/**
 * Trait for classes that utilize form_builder service.
 */
trait FormBuilderAwareTrait {

  /**
   * Get the form builder.
   *
   * @return \Drupal\Core\Form\FormBuilderInterface
   *   The form builder.
   */
  protected function getFormBuilder() {
    return \Drupal::formBuilder();
  }

}
