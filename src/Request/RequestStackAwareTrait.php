<?php

namespace Drupal\awareness\Request;

/**
 * Trait for classes that utilize request_stack service.
 */
trait RequestStackAwareTrait {

  /**
   * Get the request stack.
   *
   * @return \Symfony\Component\HttpFoundation\RequestStack
   *   The request stack.
   */
  protected function getRequestStack() {
    return \Drupal::requestStack();
  }

}
