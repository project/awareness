<?php

namespace Drupal\awareness\File;

/**
 * Trait for classes that utilize file.repository service.
 */
trait FileRepositoryAwareTrait {

  /**
   * Get the file repository service.
   *
   * @return \Drupal\file\FileRepositoryInterface
   *   The file repository service.
   */
  protected function getFileRepository() {
    return \Drupal::service('file.repository');
  }

}
