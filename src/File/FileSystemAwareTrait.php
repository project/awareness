<?php

namespace Drupal\awareness\File;

/**
 * Trait for classes that utilize file_system service.
 */
trait FileSystemAwareTrait {

  /**
   * Get the file system service.
   *
   * @return \Drupal\Core\File\FileSystemInterface
   *   The file system service.
   */
  protected function getFileSystem() {
    return \Drupal::service('file_system');
  }

}
