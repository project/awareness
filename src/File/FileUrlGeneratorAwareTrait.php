<?php

namespace Drupal\awareness\File;

/**
 * Trait for classes that utilize the file_url_generator service.
 */
trait FileUrlGeneratorAwareTrait {

  /**
   * Get the file URL generator.
   *
   * @return \Drupal\Core\File\FileUrlGeneratorInterface
   *   The file URL generator.
   */
  protected function getFileUrlGenerator() {
    return \Drupal::service('file_url_generator');
  }

}
