<?php

namespace Drupal\awareness\Extension;

/**
 * Trait for classes that utilize theme.manager service.
 */
trait ThemeManagerAwareTrait {

  /**
   * Get the theme manager.
   *
   * @return \Drupal\Core\Theme\ThemeManagerInterface
   *   The theme manager.
   */
  protected function getThemeManager() {
    return \Drupal::theme();
  }

}
