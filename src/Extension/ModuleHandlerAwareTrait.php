<?php

namespace Drupal\awareness\Extension;

/**
 * Trait for classes that utilize module_handler service.
 */
trait ModuleHandlerAwareTrait {

  /**
   * Get the module handler.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler.
   */
  protected function getModuleHandler() {
    return \Drupal::moduleHandler();
  }

}
