<?php

namespace Drupal\awareness\Session;

/**
 * Trait for classes that utilize current_user service.
 */
trait CurrentUserAwareTrait {

  /**
   * Get the current user.
   *
   * @return \Drupal\Core\Session\AccountProxyInterface
   *   The current user.
   */
  protected function getCurrentUser() {
    return \Drupal::currentUser();
  }

}
