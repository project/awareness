<?php

namespace Drupal\awareness\Settings;

/**
 * Trait for classes that utilize the settings service.
 */
trait SettingsAwareTrait {

  /**
   * Get the settings.
   *
   * @return \Drupal\Core\Site\Settings
   *   The settings.
   */
  protected function getSettings() {
    return \Drupal::service('settings');
  }

}
