<?php

declare(strict_types = 1);

namespace Drupal\awareness\Utility;

use \Drupal\Component\Utility\EmailValidatorInterface;

/**
 * Trait for classes that utilize the email.validator service.
 */
trait EmailValidatorAwareTrait {

  /**
   * Get the email.validator service.
   *
   * @return \Drupal\Component\Utility\EmailValidatorInterface
   *   The email.validator service.
   */
  protected function getEmailValidator(): EmailValidatorInterface {
    return \Drupal::service('email.validator');
  }

}
