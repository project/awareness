<?php

namespace Drupal\awareness\Http;

use GuzzleHttp\ClientInterface;

/**
 * Trait for classes that utilize the http_client service.
 */
trait HttpClientAwareTrait {

  /**
   * Get the HTTP client.
   *
   * @return \GuzzleHttp\Client
   *   The HTTP client.
   */
  protected function getHttpClient() {
    return \Drupal::httpClient();
  }

}
