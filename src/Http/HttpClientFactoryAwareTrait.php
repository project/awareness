<?php

namespace Drupal\awareness\Http;

/**
 * Trait for classes that utilize the http_client_factory service.
 */
trait HttpClientFactoryAwareTrait {

  /**
   * Get the HTTP client factory service.
   *
   * @return \Drupal\Core\Http\ClientFactory
   *   The HTTP client factory service.
   */
  protected function getHttpClientFactory() {
    return \Drupal::service('http_client_factory');
  }

}
