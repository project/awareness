<?php

declare(strict_types = 1);

namespace Drupal\awareness\Pager;

use \Drupal\Core\Pager\PagerManagerInterface;

/**
 * Trait for classes that utilize the pager.manager service.
 */
trait PagerManagerAwareTrait {

  /**
   * Get the pager.manager service.
   *
   * @return \Drupal\Core\Pager\PagerManagerInterface
   *   The pager.manager service.
   */
  protected function getPagerManager(): PagerManagerInterface {
    return \Drupal::service('pager.manager');
  }

}
