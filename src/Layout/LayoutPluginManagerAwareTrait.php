<?php declare(strict_types = 1);

namespace Drupal\awareness\Layout;

use \Drupal\Core\Layout\LayoutPluginManagerInterface;

/**
 * Trait for classes that utilize the plugin.manager.core.layout service.
 */
trait LayoutPluginManagerAwareTrait {

  /**
   * Get the plugin.manager.core.layout service.
   *
   * @return \Drupal\Core\Layout\LayoutPluginManagerInterface
   *   The plugin.manager.core.layout service.
   */
  protected function getLayoutPluginManager(): LayoutPluginManagerInterface {
    return \Drupal::service('plugin.manager.core.layout');
  }

}
