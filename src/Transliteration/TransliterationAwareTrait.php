<?php

namespace Drupal\awareness\Transliteration;

/**
 * Trait for classes that utilize the transliteration service.
 */
trait TransliterationAwareTrait {

  /**
   * Get the transliteration service.
   *
   * @return \Drupal\Component\Transliteration\TransliterationInterface
   *   The transliteration service.
   */
  protected function getTransliteration() {
    return \Drupal::service('transliteration');
  }

}
