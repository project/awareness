<?php

declare(strict_types = 1);

namespace Drupal\awareness\Entity;

use \Drupal\Core\Entity\EntityTypeBundleInfoInterface;

/**
 * Trait for classes that utilize the entity_type.bundle.info service.
 */
trait EntityTypeBundleInfoAwareTrait {

  /**
   * Get the entity_type.bundle.info service.
   *
   * @return \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   *   The entity_type.bundle.info service.
   */
  protected function getEntityTypeBundleInfo(): EntityTypeBundleInfoInterface {
    return \Drupal::service('entity_type.bundle.info');
  }

}
