<?php

namespace Drupal\awareness\Entity;

/**
 * Trait for classes that utilize entity.repository service.
 */
trait EntityRepositoryAwareTrait {

  /**
   * Get the entity repository interface.
   *
   * @return \Drupal\Core\Entity\EntityRepositoryInterface
   *   The entity repository interface.
   */
  protected function getEntityRepository() {
    return \Drupal::service('entity.repository');
  }

}
