<?php

namespace Drupal\awareness\Entity;

/**
 * Trait for classes that utilize entity_type.manager service.
 */
trait EntityTypeManagerAwareTrait {

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  protected function getEntityTypeManager() {
    return \Drupal::entityTypeManager();
  }

  /**
   * Returns the entity query object for this entity type.
   *
   * @param string $entity_type
   *   The entity type (for example, node) for which the query object should be
   *   returned.
   * @param string $conjunction
   *   (optional) Either 'AND' if all conditions in the query need to apply, or
   *   'OR' if any of them is sufficient. Defaults to 'AND'.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query object that can query the given entity type.
   */
  protected function getEntityQuery($entity_type, $conjunction = 'AND') {
    return \Drupal::entityQuery($entity_type, $conjunction);
  }

}
