<?php

declare(strict_types = 1);

namespace Drupal\awareness\Entity;

use \Drupal\Core\Entity\EntityFormBuilderInterface;

/**
 * Trait for classes that utilize the entity.form_builder service.
 */
trait EntityFormBuilderAwareTrait {

  /**
   * Get the entity.form_builder service.
   *
   * @return \Drupal\Core\Entity\EntityFormBuilderInterface
   *   The entity.form_builder service.
   */
  protected function getEntityFormBuilder(): EntityFormBuilderInterface {
    return \Drupal::service('entity.form_builder');
  }

}
