<?php

declare(strict_types = 1);

namespace Drupal\awareness\Entity;

use \Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Trait for classes that utilize the entity_field.manager service.
 */
trait EntityFieldManagerAwareTrait {

  /**
   * Get the entity_field.manager service.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   *   The entity_field.manager service.
   */
  protected function getEntityFieldManager(): EntityFieldManagerInterface {
    return \Drupal::service('entity_field.manager');
  }

}
